package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import com.example.demo.dto.PostDTO;
import com.example.demo.entity.Post;

@Service
public class PostService implements IPostService{

	@PersistenceContext
	private EntityManager entityManager;

	private int pageSize = 25; // In real life would be configurable

	public List<PostDTO> listPostTitlesAndTopics(int pageNumber) {
		@SuppressWarnings("unchecked")
		List<Post> posts = entityManager.createQuery("SELECT p FROM Post p").setFirstResult((pageNumber - 1) * pageSize)
				.setMaxResults(pageSize).getResultList();
		
		List<PostDTO> result = new ArrayList(posts.size());

		for (Post post : posts) {
			PostDTO postDto = new PostDTO();
			postDto.setId(post.getId());
			postDto.setTitle(post.getTitle());
			postDto.setTopicName(post.getTopic().getName());
			result.add(postDto);
		}

		return result;
	}
}
