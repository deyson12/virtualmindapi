package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.PostDTO;

public interface IPostService {
	
	public List<PostDTO> listPostTitlesAndTopics(int pageNumber);

}
