package com.example.demo.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.TopicDTO;
import com.example.demo.entity.Topic;
import com.example.demo.exception.EntityNotFoundException;

@Service
public class TopicService implements ITopicService{
    
	@PersistenceContext
    private EntityManager entityManager;

	@Transactional
    public void updateTopic(long topicId, TopicDTO updated) throws EntityNotFoundException {
        Topic topic = entityManager.find(Topic.class, topicId);
        if (topic != null) {
             topic.setName(updated.getName());
        } else {
            throw new EntityNotFoundException(Topic.class, topicId);
        }
    }
}
