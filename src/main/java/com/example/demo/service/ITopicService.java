package com.example.demo.service;

import com.example.demo.dto.TopicDTO;
import com.example.demo.exception.EntityNotFoundException;

public interface ITopicService {
	
	void updateTopic(long topicId, TopicDTO updated) throws EntityNotFoundException;

}
