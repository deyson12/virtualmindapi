package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.dto.TopicDTO;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.service.ITopicService;

@RestController
public class TopicController {
	
	@Autowired
	private ITopicService topicService;
	
	@PatchMapping("/topic/{id}")
	public ResponseEntity<String> updateTopic(@PathVariable long id, @RequestParam String name) {
		
		TopicDTO topidDTO = new TopicDTO();
		topidDTO.setName(name);
		
		try {
			topicService.updateTopic(id, topidDTO);
		} catch (EntityNotFoundException e) {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Topic Not Found", e);
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}	
}
