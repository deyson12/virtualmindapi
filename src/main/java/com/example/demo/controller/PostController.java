package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PostDTO;
import com.example.demo.service.IPostService;

@RestController
public class PostController {
	
	@Autowired
	private IPostService postService;

	@GetMapping("/post")
	public List<PostDTO> hello(@RequestParam int pageNumber) {
		
		List<PostDTO> listPost = postService.listPostTitlesAndTopics(pageNumber);
		
		return listPost;
	}	
}
