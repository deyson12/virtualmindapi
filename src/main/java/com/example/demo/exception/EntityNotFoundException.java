package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.demo.entity.Topic;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Topic")
public class EntityNotFoundException extends Exception {

	public EntityNotFoundException(Class<Topic> class1, long topicId) {
		// TODO Auto-generated constructor stub
		
	}

}
